#include "my_adapt.h"
#include "main.h"
#include "stdio.h"

uint8_t Rx_Flag = 0;
uint16_t Rx_Len = 0;
uint8_t Rx_Buf[Rx_Max] = {0};

/* 对打印的支持需要实现 fputc 函数 */
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&PRINT_UART, (uint8_t *)&ch, 1, 0xFFFF);
    return ch;
}

/* 基本的初始化 */
void basic_init(void)
{
    HAL_UART_Receive_DMA(&DATA_UART, Rx_Buf, Rx_Max);
    __HAL_UART_ENABLE_IT(&DATA_UART, UART_IT_IDLE);
}

/**
 * @brief   进入串口中断之后执行的第一函数
 *
 */
void HAL_UART_IRQHandler_before(void)
{
    uint32_t temp;
    if ((__HAL_UART_GET_FLAG(&DATA_UART, UART_FLAG_IDLE) != RESET)) {
        /*清除状态寄存器和串口数据寄存器*/
        __HAL_UART_CLEAR_IDLEFLAG(&DATA_UART);

        /*失能DMA接收*/
        HAL_UART_DMAStop(&DATA_UART);

        /*读取接收长度，总大小-剩余大小*/
        temp = DATA_UART.hdmarx->Instance->CNDTR;
        Rx_Len = Rx_Max - temp;

        /*接收标志位置1*/
        Rx_Flag = 1;

        /*使能接收DMA接收*/
        HAL_UART_Receive_DMA(&DATA_UART, Rx_Buf, Rx_Max);
    }
}
