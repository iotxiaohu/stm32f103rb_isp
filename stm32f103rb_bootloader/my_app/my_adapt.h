#ifndef _ADAPT_H_
#define _ADAPT_H_

#include "main.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

#define Rx_Max  1024
extern uint8_t  Rx_Flag;
extern uint16_t Rx_Len;
extern uint8_t  Rx_Buf[Rx_Max];

#define DATA_UART        huart2
#define PRINT_UART       huart1

#define DEBUG
#ifdef DEBUG
#define LOG(format,args...)    printf(format,##args)
#else
#define LOG(format,args...)    ((void)0)
#endif


void basic_init(void);
void HAL_UART_IRQHandler_before(void);

#endif
