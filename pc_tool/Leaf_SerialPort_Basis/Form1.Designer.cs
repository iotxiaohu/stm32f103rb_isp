﻿namespace Leaf_SerialPort_Basis
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label_Port = new System.Windows.Forms.Label();
            this.label_BaudRate = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox_BaudRate = new System.Windows.Forms.ComboBox();
            this.comboBox_Port = new System.Windows.Forms.ComboBox();
            this.button_About = new System.Windows.Forms.Button();
            this.button_OpenFile = new System.Windows.Forms.Button();
            this.textBox_File = new System.Windows.Forms.TextBox();
            this.button_Update = new System.Windows.Forms.Button();
            this.richTextBox_debug1 = new System.Windows.Forms.RichTextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_Port
            // 
            this.label_Port.AutoSize = true;
            this.label_Port.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_Port.Location = new System.Drawing.Point(15, 13);
            this.label_Port.Name = "label_Port";
            this.label_Port.Size = new System.Drawing.Size(57, 21);
            this.label_Port.TabIndex = 0;
            this.label_Port.Text = "端   口";
            // 
            // label_BaudRate
            // 
            this.label_BaudRate.AutoSize = true;
            this.label_BaudRate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_BaudRate.Location = new System.Drawing.Point(14, 59);
            this.label_BaudRate.Name = "label_BaudRate";
            this.label_BaudRate.Size = new System.Drawing.Size(58, 21);
            this.label_BaudRate.TabIndex = 1;
            this.label_BaudRate.Text = "波特率";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightBlue;
            this.panel1.Controls.Add(this.comboBox_BaudRate);
            this.panel1.Controls.Add(this.comboBox_Port);
            this.panel1.Controls.Add(this.label_Port);
            this.panel1.Controls.Add(this.label_BaudRate);
            this.panel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel1.Location = new System.Drawing.Point(13, 207);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(195, 94);
            this.panel1.TabIndex = 2;
            // 
            // comboBox_BaudRate
            // 
            this.comboBox_BaudRate.BackColor = System.Drawing.Color.White;
            this.comboBox_BaudRate.FormattingEnabled = true;
            this.comboBox_BaudRate.Location = new System.Drawing.Point(78, 57);
            this.comboBox_BaudRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox_BaudRate.Name = "comboBox_BaudRate";
            this.comboBox_BaudRate.Size = new System.Drawing.Size(100, 25);
            this.comboBox_BaudRate.TabIndex = 4;
            this.comboBox_BaudRate.Text = "115200";
            // 
            // comboBox_Port
            // 
            this.comboBox_Port.Location = new System.Drawing.Point(78, 11);
            this.comboBox_Port.Name = "comboBox_Port";
            this.comboBox_Port.Size = new System.Drawing.Size(100, 25);
            this.comboBox_Port.TabIndex = 5;
            this.comboBox_Port.DropDown += new System.EventHandler(this.comboBox_Port_DropDown);
            // 
            // button_About
            // 
            this.button_About.BackColor = System.Drawing.Color.LightBlue;
            this.button_About.Location = new System.Drawing.Point(403, 309);
            this.button_About.Name = "button_About";
            this.button_About.Size = new System.Drawing.Size(72, 27);
            this.button_About.TabIndex = 19;
            this.button_About.Text = "关于";
            this.button_About.UseVisualStyleBackColor = false;
            this.button_About.Click += new System.EventHandler(this.Button_About_Click);
            // 
            // button_OpenFile
            // 
            this.button_OpenFile.BackColor = System.Drawing.Color.LightBlue;
            this.button_OpenFile.Location = new System.Drawing.Point(403, 160);
            this.button_OpenFile.Name = "button_OpenFile";
            this.button_OpenFile.Size = new System.Drawing.Size(72, 27);
            this.button_OpenFile.TabIndex = 20;
            this.button_OpenFile.Text = "打开文件";
            this.button_OpenFile.UseVisualStyleBackColor = false;
            this.button_OpenFile.Click += new System.EventHandler(this.button_OpenFile_Click);
            // 
            // textBox_File
            // 
            this.textBox_File.BackColor = System.Drawing.Color.White;
            this.textBox_File.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox_File.Location = new System.Drawing.Point(13, 162);
            this.textBox_File.Name = "textBox_File";
            this.textBox_File.Size = new System.Drawing.Size(387, 23);
            this.textBox_File.TabIndex = 21;
            // 
            // button_Update
            // 
            this.button_Update.BackColor = System.Drawing.Color.LightBlue;
            this.button_Update.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_Update.Location = new System.Drawing.Point(211, 207);
            this.button_Update.Name = "button_Update";
            this.button_Update.Size = new System.Drawing.Size(264, 94);
            this.button_Update.TabIndex = 20;
            this.button_Update.Text = "升级";
            this.button_Update.UseVisualStyleBackColor = false;
            this.button_Update.Click += new System.EventHandler(this.button_Update_Click);
            // 
            // richTextBox_debug1
            // 
            this.richTextBox_debug1.BackColor = System.Drawing.Color.LightBlue;
            this.richTextBox_debug1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_debug1.Name = "richTextBox_debug1";
            this.richTextBox_debug1.Size = new System.Drawing.Size(463, 128);
            this.richTextBox_debug1.TabIndex = 11;
            this.richTextBox_debug1.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(487, 346);
            this.Controls.Add(this.richTextBox_debug1);
            this.Controls.Add(this.textBox_File);
            this.Controls.Add(this.button_Update);
            this.Controls.Add(this.button_OpenFile);
            this.Controls.Add(this.button_About);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "ioxiaohu的升级测试程序";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Port;
        private System.Windows.Forms.Label label_BaudRate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox_BaudRate;
        private System.Windows.Forms.ComboBox comboBox_Port;
        private System.Windows.Forms.Button button_About;
        private System.Windows.Forms.Button button_OpenFile;
        private System.Windows.Forms.TextBox textBox_File;
        private System.Windows.Forms.Button button_Update;
        private System.Windows.Forms.RichTextBox richTextBox_debug1;
    }
}

